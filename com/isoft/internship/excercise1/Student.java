package com.isoft.internship.excercise1;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public class Student
{
	private String name;
	private int age;
	private String gender;
	private int entityId;
	private double averageGrade;
	private Random rand = new SecureRandom();

	public Student(String name, int age, String gender) throws NoSuchAlgorithmException
	{
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.averageGrade = generateRandomGrade();
		this.rand = SecureRandom.getInstanceStrong();
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getAge()
	{
		return age;
	}

	public void setAge(int age)
	{
		this.age = age;
	}

	public String getGender()
	{
		return gender;
	}

	public void setGender(String gender)
	{
		this.gender = gender;
	}

	public int getEntityId()
	{
		return entityId;
	}

	public void setEntityId(int entityId)
	{
		this.entityId = entityId;
	}

	public double getGrade()
	{
		return averageGrade;
	}

	public void setGrade(double grade)
	{
		this.averageGrade = grade;
	}

	private double generateRandomGrade()
	{
		double averageGrade = 2 + (6 - 2) * this.rand.nextDouble();
		double roundedAverageGrade = round(averageGrade, 2);
		return roundedAverageGrade;
	}

	private double round(double value, int decimal)
	{
		BigDecimal rounded = new BigDecimal(value);
		return rounded.setScale(decimal, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	public double calculatePayment()
	{
		double totalPayment = 0;
		int tax = 0;
		if (University.isUniversityStudent(this))
		{
			tax = University.getTax();
			totalPayment = (this.age / this.averageGrade * 100) + tax;
			return totalPayment;
		} 
		else
		{
			tax = School.getTax();
			totalPayment = (this.age / this.averageGrade * 100) + tax;
			return totalPayment;
		}
	}
}
