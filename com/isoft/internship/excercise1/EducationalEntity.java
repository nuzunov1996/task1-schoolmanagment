package com.isoft.internship.excercise1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class EducationalEntity
{
	private int id;
	private String name;
	private String address;
	private ArrayList<Student> students;

	public EducationalEntity(int id, String name, String address)
	{
			this.id = id;
			this.name = name;
			this.address = address;
			this.students = new ArrayList<Student>();
	}
	
	public EducationalEntity(int id, String name, String address, ArrayList<Student> students)
	{
			this.id = id;
			this.name = name;
			this.address = address;
			this.students = students;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getAddress()
	{
		return address;
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public ArrayList<Student> getStudents()
	{
		return students;
	}

	public void setStudents(ArrayList<Student> students)
	{
		this.students = students;
	}
	
	protected void sayHello(Student student)
	{
		System.out.println("Hello " + student.getName() + " and welcome to " + getName());
	}

	public void addStudent(Student student)
	{
		student.setEntityId(getId());
		ArrayList<Student> students = getStudents();
		students.add(student);
		setStudents(students);
		sayHello(student);
	}
	
	public void addStudents(ArrayList<Student> students)
	{
		ArrayList<Student> currensStudents = getStudents();
		
		for (Student student : students)
		{
			student.setEntityId(getId());
			sayHello(student);
		}
		currensStudents.addAll(students);
		setStudents(students);
	}

	public void printAverageGradeOfStudents()
	{
		ArrayList<Student> students = getStudents();
		
		for (Student student : students)
		{
			System.out.println(student.getName() + " Grade: " + student.getGrade());
		}
	}

	public Student getTopPerformingStudent()
	{
		ArrayList<Student> students = new ArrayList<>(getStudents());
		Student student = Collections.max(students, Comparator.comparing(s -> s.getGrade()));
		return student;
	}

	
	public static Student getTopPerformingStudent(ArrayList<Student> allUniversityStudents)
	{
		Student student = Collections.max(allUniversityStudents, Comparator.comparing(s -> s.getGrade()));
		return student;
	}

	public Student getTopPerformingStudentByGender(String gender)
	{
		ArrayList<Student> students = new ArrayList<>(getStudents());
		
		if (gender.toLowerCase() == "male")
		{
			ArrayList<Student> maleStudents = new ArrayList<Student>();
			for (Student student : students)
			{
				if (student.getGender() == "male")
				{
					maleStudents.add(student);
				}
			}
			Student stutend = Collections.max(maleStudents, Comparator.comparing(s -> s.getGrade()));
			return stutend;
		}
		else if(gender.toLowerCase() == "female")
		{
			ArrayList<Student> femaleStudents = new ArrayList<Student>();
			for (Student student : students)
			{
				if (student.getGender().toLowerCase() == "female")
				{
					femaleStudents.add(student);
				}
			}
			Student stutend = Collections.max(femaleStudents, Comparator.comparing(s -> s.getGrade()));
			return stutend;
		}
		return null;
	}
	
	public static Student getTopPerformingStudentByGender(ArrayList<Student> allUniversityStudents, String gender)
	{
		if (gender.toLowerCase() == "male")
		{
			ArrayList<Student> maleStudents = new ArrayList<Student>();
			for (Student student : allUniversityStudents)
			{
				if (student.getGender() == "male")
				{
					maleStudents.add(student);
				}
			}
			Student stutend = Collections.max(maleStudents, Comparator.comparing(s -> s.getGrade()));
			return stutend;
		}
		else if(gender.toLowerCase() == "female")
		{
			ArrayList<Student> femaleStudents = new ArrayList<Student>();
			for (Student student : allUniversityStudents)
			{
				if (student.getGender().toLowerCase() == "female")
				{
					femaleStudents.add(student);
				}
			}
			Student stutend = Collections.max(femaleStudents, Comparator.comparing(s -> s.getGrade()));
			return stutend;
		}
		return null;
	}

	public double getTotalIncome()
	{
		double totalIncome = 0;
		for (Student student : getStudents())
		{
			totalIncome =+ student.calculatePayment();
		}
		return totalIncome;
	}
	
	public static double getTotalIncome(ArrayList<EducationalEntity> educations)
	{
		double totalIncome = 0;
		for (EducationalEntity education : educations)
		{
			ArrayList<Student> students = education.getStudents();
			for (Student student : students)
			{
				totalIncome = totalIncome + student.calculatePayment();
			}
		}
		return totalIncome;
	}
	
	public static Student getTopContributor(ArrayList<Student> students)
	{		
		Student student = Collections.max(students, Comparator.comparing(s -> s.calculatePayment()));
		return student;
	}
	
	public double getAverageGrade()
	{
		double averageGrade = 0;
		double sumOfAllGrades = 0;
		for (Student student : students)
		{
			sumOfAllGrades = sumOfAllGrades + student.getGrade();
		}
		averageGrade = sumOfAllGrades/getStudents().size();
		
		return averageGrade;
	}
}
