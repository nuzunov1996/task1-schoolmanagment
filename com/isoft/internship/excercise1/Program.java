package com.isoft.internship.excercise1;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class Program
{
	private static final int START = 0;
	private static final int END = 2;
	
	public static void main(String[] args)
	{
		//UUID.randomUUID().ToString();		
		ArrayList<EducationalEntity> educationalEntities = new ArrayList<EducationalEntity>();
		ArrayList<EducationalEntity> schools = new ArrayList<EducationalEntity>();
		ArrayList<EducationalEntity> universities = new ArrayList<EducationalEntity>();
		
		for (int i = START; i < END; i++)
		{
			University entity = new University(i, RandomOperations.getRadnomUniversityName(), RandomOperations.getRadnomCities());
			entity.addStudents(generateStudents(10));
			System.out.println();
			educationalEntities.add(entity);
			universities.add(entity);
		}
		
		for (int i = END + 1; i < END + 4; i++)
		{
			School entity = new School(i, RandomOperations.getRadnomSchoolName(), RandomOperations.getRadnomCities());
			entity.addStudents(generateStudents(10));
			System.out.println();
			educationalEntities.add(entity);
			schools.add(entity);
		}
		
		//Average grade of all students per entity(School/University)
		for (EducationalEntity educationalEntity : educationalEntities)
		{
			System.out.println("University/School name: " + educationalEntity.getName() + " and average grade of all students: " 
					+ educationalEntity.getAverageGrade());
		}
		System.out.println();
		
		//Top performing student from all universities and schools
		Student topStudent = EducationalEntity.getTopPerformingStudent(getGetStudentsFromEducation(educationalEntities));
		System.out.println("Top student from all educational entities: " + topStudent.getGrade());
		Student topStudentFromAllUniversities = University.getTopPerformingStudent(getGetStudentsFromEducation(universities));
		System.out.println("Top student from universities: " + topStudentFromAllUniversities.getGrade() );
		Student topStudentFromAllSchools = School.getTopPerformingStudent(getGetStudentsFromEducation(schools));
		System.out.println("Top student from schools: " + topStudentFromAllSchools.getGrade() +'\n');
		
		//Top performing student from all universities and schools by gender
		Student topMaleStudent = EducationalEntity.getTopPerformingStudentByGender(getGetStudentsFromEducation(educationalEntities), "male");
		System.out.println("Top male student from all educational entities: " + topMaleStudent.getGrade());
		Student topMaleStudentFromAllUniversities = University.getTopPerformingStudentByGender(getGetStudentsFromEducation(universities), "male");
		System.out.println("Top male student from all universities: " + topMaleStudentFromAllUniversities.getGrade());
		Student topMaleStudentFromAllSchools = University.getTopPerformingStudentByGender(getGetStudentsFromEducation(schools), "male");
		System.out.println("Top male student from all schools: " + topMaleStudentFromAllSchools.getGrade() + "\n");
		
		double totalIncome = 0; 
		totalIncome = EducationalEntity.getTotalIncome(educationalEntities);
		System.out.println("total income from all educations: " + totalIncome + "\n");
		
		//Print the name of the top contributor of all universities and schools
		Student topContriburor = EducationalEntity.getTopContributor(getGetStudentsFromEducation(educationalEntities));
		System.out.println("The name of top contributor of all universities and schools: " + topContriburor.getName() + "\n");
	}

	static ArrayList<Student> generateStudents(int countOfStudents)
	{
		ArrayList<Student> students = new ArrayList<Student>();
		for (int i = 0; i < countOfStudents / 2; i++)
		{
			try
			{
				students.add(new Student(RandomOperations.getRadnomMaleName(),
						RandomOperations.generateRandomNumber(5, 100), "male"));
			} catch (NoSuchAlgorithmException ignored)
			{

			}
		}
		for (int i = 0; i < countOfStudents / 2; i++)
		{
			try
			{
				students.add(new Student(RandomOperations.getRadnomFemaleName(),
						RandomOperations.generateRandomNumber(5, 100), "female"));
			} catch (NoSuchAlgorithmException ignored)
			{

			}
		}

		return students;
	}

	public static ArrayList<Student> getGetStudentsFromEducation(ArrayList<EducationalEntity> educations)
	{
		ArrayList<Student> students = new ArrayList<Student>();

		for (EducationalEntity university : educations)
		{
			students.addAll(university.getStudents());
		}

		return students;
	}
}
