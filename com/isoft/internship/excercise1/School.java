package com.isoft.internship.excercise1;
import java.util.ArrayList;
import java.util.HashSet;

public class School extends EducationalEntity
{
	private static final int TAX = 100;
	private static HashSet<Integer> ids = new HashSet<Integer>();

	public School(int id, String name, String address)
	{
		super(id, name, address);
		ids.add(id);
	}
	
	public School(int id, String name, String address, ArrayList<Student> students)
	{
		super(id, name, address, students);
		// TODO Auto-generated constructor stub
	}
	
	public static boolean isSchoolStudent(Student student)
	{
		boolean isSchoolStudent = ids.contains(student.getEntityId());
		if (isSchoolStudent)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	public static int getTax()
	{
		return TAX;
	}
}
