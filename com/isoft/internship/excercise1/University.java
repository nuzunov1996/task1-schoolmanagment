package com.isoft.internship.excercise1;
import java.util.ArrayList;
import java.util.HashSet;

public class University extends EducationalEntity
{
	private static final int TAX = 100;
	private static HashSet<Integer> ids = new HashSet<Integer>();

	public University(int id, String name, String address)
	{
		super(id, name, address);
		ids.add(id);
	}

	public University(int id, String name, String address, ArrayList<Student> students)
	{
		super(id, name, address, students);
	}

	public static boolean isUniversityStudent(Student student)
	{
		return ids.contains(student.getEntityId());		
	}
	
	public static int getTax()
	{
		return TAX;
	}
}
