package com.isoft.internship.excercise1;
import java.security.SecureRandom;
import java.util.Random;

public class RandomOperations
{
	private static final String[] FEMALE_NAMES = { "Ivana", "Kristina", "Boqna", "Ioana", "Kameliq", "Antoniq" };
	private static final String[] MALE_NAMES = { "Ivan", "Kristian", "Boqn", "Ioan", "Mitko", "Anton" };
	private static final String[] GENDER = { "male", "fenale" };
	private static final String[] UNIVERSITY_NAMES = { "Technical", "Free University" };
	private static final String[] SCHOOL_NAMES = { "High School Of Electrical Engineering", "Mathematics High School",
			"Professional Technical School" };
	private static final String[] CITIES = { "Varna", "Varna", "Burgas", "Sofiq" };
	private static Random rand = new SecureRandom();


	private static int generateRandomNumber(int maxValue)
	{
		int number = rand.nextInt((maxValue) + 1);
		return number;
	}

	public static int generateRandomNumber(int min, int max)
	{
		int number = rand.nextInt((max - min) + 1) + max;
		return number;
	}

	public static String getRadnomFemaleName()
	{
		int randomNumber = generateRandomNumber(FEMALE_NAMES.length - 1);
		return FEMALE_NAMES[randomNumber];
	}

	public static String getRadnomMaleName()
	{
		int randomNumber = generateRandomNumber(MALE_NAMES.length - 1);
		return MALE_NAMES[randomNumber];
	}
	
	public static String getRadnomGender()
	{
		int randomNumber = generateRandomNumber(GENDER.length -1);
		return GENDER[randomNumber];
	}
	
	public static String getRadnomSchoolName()
	{
		int randomNumber = generateRandomNumber(SCHOOL_NAMES.length -1);
		return SCHOOL_NAMES[randomNumber];
	}
	
	public static String getRadnomUniversityName()
	{
		int randomNumber = generateRandomNumber(UNIVERSITY_NAMES.length -1);
		return UNIVERSITY_NAMES[randomNumber];
	}
	
	public static String getRadnomCities()
	{
		int randomNumber = generateRandomNumber(CITIES.length -1);
		return CITIES[randomNumber];
	}
}
